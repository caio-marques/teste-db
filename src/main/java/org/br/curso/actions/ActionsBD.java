package org.br.curso.actions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


import com.br.inmetrics.frm.helpers.PropertyHelper;

public class ActionsBD {
	
	private Connection connection;
	public static int lastIdCreated;
	private static ActionsBD instance;


	public ActionsBD() {
		if(instance == null)
			instance = this;
	}
	
	public static ActionsBD createOrInstance() {
		if(instance == null)
			new ActionsBD();
		
		return instance;
	}
	
	public int lastIdCreated() {
		return lastIdCreated;
	}
	
	
	public boolean isConnected() {
		return connection != null;
	}
	
	public void initializeConnecion() throws SQLException {
		connection = DriverManager.getConnection(PropertyHelper.getProperty("db.connections.string"));
	}
	
	public static Connection getConnection() {
		
		try {
			if(instance != null && instance.connection != null)
				return instance.connection;
			return DriverManager.getConnection(PropertyHelper.getProperty("db.connections.string"));
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}




}
