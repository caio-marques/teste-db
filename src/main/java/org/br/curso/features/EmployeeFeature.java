package org.br.curso.features;

import java.util.concurrent.ExecutionException;
import static com.br.inmetrics.frm.bdd.Gherkin.given_;

import com.br.inmetrics.frm.bdd.Feature;
import com.br.inmetrics.frm.bdd.Scenario;

@Feature("EmployeeFeature")
public class EmployeeFeature {


		@SuppressWarnings("static-access")
		@Scenario("Validar a inserção de um empregado na base")
		public void validarInsertEmpregadoBase() throws ExecutionException {
			given_("Dado que faça a inserção de um novo empregado").
			when_("Quando fizer a busca do empregado").
			then_("Então deverá retornar o empregado inserido").
			execute_();
			
		}
		
		@SuppressWarnings("static-access")
		@Scenario("Validar a alteração de um empregado na base")
		public void validarUpdateEmpregadoBase() throws ExecutionException {
			given_("Dado que faça a alteração de um novo empregado").
			when_("Quando fizer a busca do empregado").
			then_("Então deverá retornar o empregado alterado").
			execute_();
			
		}
		
		@SuppressWarnings("static-access")
		@Scenario("Validar a exclusão de um empregado na base")
		public void validarDeleteEmpregadoBase() throws ExecutionException {
			given_("Dado que faça a exclusão de um novo empregado").
			when_("Quando fizer a busca do empregado").
			then_("Então deverá retornar 0 registros de empregados").
			execute_();
			
		}

		
	}


