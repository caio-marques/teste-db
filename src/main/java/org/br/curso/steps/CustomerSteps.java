package org.br.curso.steps;

import static com.br.inmetrics.frm.helpers.DataTableHelper.getDt_;

import org.br.curso.actions.ActionsBD;
import org.br.curso.dao.*;
import org.br.curso.to.CustomerTo;
import org.testng.Assert;


import com.br.inmetrics.frm.bdd.Step;

public class CustomerSteps {
	ActionsBD actionsDB = ActionsBD.createOrInstance();
	CustomerDao customerDao = new CustomerDao();
	private static CustomerTo tempCustomer = null;
	private static int rowsModified = 0;
	


	@Step("Dado que faça a inserção de um novo cliente")
	public void dado_que_faca_a_insercao_de_um_novo_cliente() {
		
		try {
			if(!actionsDB.isConnected()) actionsDB.initializeConnecion();
			customerDao.insertCustomer(getDt_().getStringOf("CompanyName"),
					                   getDt_().getStringOf("ContactName"), 
					                   getDt_().getStringOf("ContactTitle"), 
					                   getDt_().getStringOf("Address"),
					                   getDt_().getStringOf("City"),
					                   getDt_().getStringOf("State"));
		} catch (Exception e) {
			Assert.fail("Erro na inserção do cliente", e);
		}
		
	}
	
	@Step("Dado que faça a alteração de um cliente existente")
	public void dado_que_faca_a_alteracao_de_um_cliente_existente() {
		
		try {
			if(!actionsDB.isConnected()) actionsDB.initializeConnecion();
			rowsModified = customerDao.updateCustomer(actionsDB.lastIdCreated(), getDt_().getStringOf("CompanyName"),
	                   getDt_().getStringOf("ContactName"), 
	                   getDt_().getStringOf("ContactTitle"), 
	                   getDt_().getStringOf("Address"),
	                   getDt_().getStringOf("City"),
	                   getDt_().getStringOf("State"));
		} catch(Exception e) {
			Assert.fail("Erro na alteração de um cliente existente", e);
		}
		
	}
	
	@Step("Dado que faça a exclusão de um cliente existente")
	public void dado_que_faca_a_exclusao_de_um_cliente_existente() {
		
		try {
			if(!actionsDB.isConnected()) actionsDB.initializeConnecion();
			rowsModified = customerDao.deleteCustomer(actionsDB.lastIdCreated());
		} catch(Exception e) {
			Assert.fail("Erro na exclusão de um cliente", e);
		}

	}
	

	
	@Step("Quando fizer a busca do cliente")
	public void quando_fizer_a_busca() {
		
		try {
			if(actionsDB.lastIdCreated() >= 0)
				tempCustomer = customerDao.getCustomer(actionsDB.lastIdCreated());
		} catch (Exception e) {
			Assert.fail("Erro na busca do cliente", e);
		}
		
	}
	
	@Step("Então deverá retornar o cliente inserido")
	public void entao_devera_retornar_o_cliente_inserido() {
	
		try {
			Assert.assertTrue(actionsDB.lastIdCreated() > 0);
			Assert.assertNotNull(tempCustomer);
			Assert.assertEquals(tempCustomer.companyName , getDt_().getStringOf("CompanyName") );
		} catch (Exception e) {
			Assert.fail("Erro na validação do retorno de um cliente inserido", e);
		}
	}
	
	
	

	

	
	@Step("Então deverá retornar o cliente alterado")
	public void entao_devera_retornar_o_cliente_alterado() {
		
		try {
			Assert.assertEquals(rowsModified, 1);
			Assert.assertNotNull(tempCustomer);
			Assert.assertEquals(tempCustomer.companyName, getDt_().getStringOf("CompanyName"));
		} catch(Exception e) {
			Assert.fail("Erro na validação do retorno de um cliente alterado", e);
		}
		
	}
	

	
	@Step("Então deverá retornar 0 registros")
	public void entao_devera_retornar_nenhum_registro() {
	
		try {
			Assert.assertEquals(rowsModified, 1);
			Assert.assertNull(tempCustomer);
		} catch(Exception e) {
			Assert.fail("Erro na validação do retorno de um cliente excluído", e);
		}
		
	}

}
