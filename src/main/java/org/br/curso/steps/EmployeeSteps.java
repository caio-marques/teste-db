package org.br.curso.steps;


import static com.br.inmetrics.frm.helpers.DataTableHelper.getDt_;

import org.br.curso.actions.ActionsBD;
import org.br.curso.dao.EmployeeDao;
import org.br.curso.to.EmployeeTo;
import org.testng.Assert;

import com.br.inmetrics.frm.bdd.Step;


public class EmployeeSteps {
	
	ActionsBD actionsDB = ActionsBD.createOrInstance();
	EmployeeDao employeeDao = new EmployeeDao();
	private static EmployeeTo tempCustomer = null;
	private static int rowsModified = 0;
	
	@Step("Dado que faça a inserção de um novo empregado")
	public void dado_que_faca_a_insercao_de_um_novo_cliente() {
		
		try {
			if(!actionsDB.isConnected()) actionsDB.initializeConnecion();
			employeeDao.insertEmployee(getDt_().getStringOf("LastName"),
					                   getDt_().getStringOf("FirstName"), 
					                   getDt_().getStringOf("Title"), 
					                   getDt_().getStringOf("Address"),
					                   getDt_().getStringOf("HireDate"));
		} catch (Exception e) {
			Assert.fail("Erro na inserção do cliente", e);
		}
		
	}
	
	@Step("Dado que faça a alteração de um novo empregado")
	public void dado_que_faca_a_alteracao_de_um_cliente_existente() {
		
		try {
			if(!actionsDB.isConnected()) actionsDB.initializeConnecion();
			rowsModified = employeeDao.updateEmployee(actionsDB.lastIdCreated(),
					   				                   getDt_().getStringOf("LastName"),
	                                                   getDt_().getStringOf("FirstName"), 
	                                                   getDt_().getStringOf("Title"), 
	                                                   getDt_().getStringOf("Address"),
	                                                   getDt_().getStringOf("HireDate"));
		} catch(Exception e) {
			Assert.fail("Erro na alteração de um cliente existente", e);
		}
		
	}
	
	@Step("Dado que faça a exclusão de um novo empregado")
	public void dado_que_faca_a_exclusao_de_um_cliente_existente() {
		
		try {
			if(!actionsDB.isConnected()) actionsDB.initializeConnecion();
			rowsModified = employeeDao.deleteEmployee(actionsDB.lastIdCreated());
		} catch(Exception e) {
			Assert.fail("Erro na exclusão de um cliente", e);
		}

	}
	

	
	@Step("Quando fizer a busca do empregado")
	public void quando_fizer_a_busca() {
		
		try {
			if(actionsDB.lastIdCreated() >= 0)
				tempCustomer = employeeDao.getEmployee(actionsDB.lastIdCreated());
		} catch (Exception e) {
			Assert.fail("Erro na busca do cliente", e);
		}
		
	}
	
	@Step("Então deverá retornar o empregado inserido")
	public void entao_devera_retornar_o_cliente_inserido() {
	
		try {
			Assert.assertTrue(actionsDB.lastIdCreated() > 0);
			Assert.assertNotNull(tempCustomer);
			Assert.assertEquals(tempCustomer.lastName , getDt_().getStringOf("LastName") );
		} catch (Exception e) {
			Assert.fail("Erro na validação do retorno de um cliente inserido", e);
		}
	}
	
	
	

	

	
	@Step("Então deverá retornar o empregado alterado")
	public void entao_devera_retornar_o_cliente_alterado() {
		
		try {
			Assert.assertEquals(rowsModified, 1);
			Assert.assertNotNull(tempCustomer);
			Assert.assertEquals(tempCustomer.lastName, getDt_().getStringOf("LastName"));
		} catch(Exception e) {
			Assert.fail("Erro na validação do retorno de um cliente alterado", e);
		}
		
	}
	

	
	@Step("Então deverá retornar 0 registros de empregados")
	public void entao_devera_retornar_nenhum_registro() {
	
		try {
			Assert.assertEquals(rowsModified, 1);
			Assert.assertNull(tempCustomer);
		} catch(Exception e) {
			Assert.fail("Erro na validação do retorno de um cliente excluído", e);
		}
		
	}

}
