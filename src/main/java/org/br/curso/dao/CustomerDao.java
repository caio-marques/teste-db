package org.br.curso.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Statement;

import org.br.curso.actions.ActionsBD;
import org.br.curso.to.CustomerTo;

import com.br.inmetrics.frm.helpers.LoggerHelper;

public class CustomerDao {

	private LoggerHelper logger = new LoggerHelper(CustomerDao.class);

	public ArrayList<CustomerTo> getCustomers() throws SQLException {
		
		ArrayList<CustomerTo> results = new ArrayList<CustomerTo>();
		
		String selectStmt = "SELECT * FROM Customers";		
		PreparedStatement stmt = ActionsBD.getConnection().prepareStatement(selectStmt);
		
		logger.info("SQL STMT:" + selectStmt);
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()) {
			results.add(new CustomerTo(rs.getInt("CustomerId"), 
					   rs.getString("CompanyName"), 
					   rs.getString("ContactName"), 
					   rs.getString("Address"),
					   rs.getString("City"),
					   rs.getString("State")));
		}
		
		return results;
		
	}
	
	public CustomerTo getCustomer(int id) throws SQLException {
		
		String selectStmt = "SELECT * FROM Customers WHERE CustomerID=?"; 
		
		PreparedStatement stmt = ActionsBD.getConnection().prepareStatement(selectStmt);
		stmt.setInt(1, id);
		
		logger.info("SQL STMT:" + selectStmt);
		ResultSet rs = stmt.executeQuery();
		
		if(rs.next()) {
			CustomerTo result = new CustomerTo(rs.getInt("CustomerId"), 
											   rs.getString("CompanyName"), 
											   rs.getString("ContactName"), 
											   rs.getString("Address"),
											   rs.getString("City"),
											   rs.getString("State"));
			
			return result;
		}
		
		return null;
	}
	
	public int insertCustomer(String companyName, 
							  String contactName,
							  String contactTitle,
							  String address,
							  String city, 
							  String state) throws SQLException{
		
		ActionsBD.lastIdCreated = -1;
		
		String insertStmt = "INSERT INTO Customers " + 
							"(CompanyName, ContactName, ContactTitle, Address, City, State) " + 
							" VALUES (?, ?, ?, ?, ?, ?) ";
		
		PreparedStatement stmt = ActionsBD.getConnection().prepareStatement(insertStmt);
		stmt.setString(1, companyName);
		stmt.setString(2, contactName);
		stmt.setString(3, contactTitle);
		stmt.setString(4, address);
		stmt.setString(5, city);
		stmt.setString(6, state);
		
		logger.info("SQL STMT:" + insertStmt);
		stmt.executeUpdate();
		
		
		ActionsBD.lastIdCreated = getLastIdCreated();
						
		return ActionsBD.lastIdCreated;
	}
	
	public int getLastIdCreated() throws SQLException {
		
		String selectStmt = "SELECT seq FROM sqlite_sequence WHERE Name='Customers'";
		Statement stmt = ActionsBD.getConnection().createStatement();		
		logger.info("SQL STMT:" + selectStmt);
		ResultSet result = stmt.executeQuery(selectStmt);
		
		if(result.next())
			return result.getInt("seq");
		
		return -1;
				
	}
	
	public int updateCustomer(int id,
			                  String companyName, 
			                  String contactName,
			                  String contactTitle,
			                  String address,
			                  String city, 
			                  String state) throws SQLException{
			 
		String updateStmt = "UPDATE Customers SET " + 
							"CompanyName=?, " +
							"ContactName=?, " +
							"Address=?, " + 
							"City=?, " + 
							"State=? " +
							"WHERE " + 
							"CustomerID=?";
		
		PreparedStatement stmt = ActionsBD.getConnection().prepareStatement(updateStmt);
		stmt.setString(1, companyName);
		stmt.setString(2, contactName);
		stmt.setString(3, address);
		stmt.setString(4, city);
		stmt.setString(5, state);
		stmt.setInt(6, id);
		
		logger.info("SQL STMT:" + updateStmt);
		
		return stmt.executeUpdate();
									
	}
	
	public int deleteCustomer(int id) throws SQLException {
		
		String deleteStmt = "DELETE FROM Customers WHERE CustomerID=?";
		
		PreparedStatement stmt = ActionsBD.getConnection().prepareStatement(deleteStmt);
		stmt.setInt(1, id);
		
		logger.info("SQL STMT:" + deleteStmt);
		
		return stmt.executeUpdate();
	}

}
