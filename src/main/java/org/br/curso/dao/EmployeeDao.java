package org.br.curso.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.br.curso.actions.ActionsBD;
import org.br.curso.to.EmployeeTo;

import com.br.inmetrics.frm.helpers.LoggerHelper;

public class EmployeeDao {


	private LoggerHelper logger = new LoggerHelper(CustomerDao.class);
//    EmployeeTo employeeTo = new EmployeeTo();

	
	public List<EmployeeTo> getEmployees() throws SQLException {
		
		List<EmployeeTo> results = new ArrayList<EmployeeTo>();
		
		String selectStmt = "SELECT * FROM Employees";		
		PreparedStatement stmt = ActionsBD.getConnection().prepareStatement(selectStmt);
		
		logger.info("SQL STMT:" + selectStmt);
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()) {
			results.add(new EmployeeTo(rs.getInt("EmployeeID"),
					                   rs.getString("LastName"),
					                   rs.getString("FirstName"), 
					                   rs.getString("Title"),
					                   rs.getString("Address"),
					                   rs.getString("HireDate")));
		}
		
		return results;
		
	}
	
	public EmployeeTo getEmployee(int id) throws SQLException {
		
		String selectStmt = "SELECT * FROM Employees WHERE EmployeeID=?"; 
		
		PreparedStatement stmt = ActionsBD.getConnection().prepareStatement(selectStmt);
		stmt.setInt(1, id);
		
		logger.info("SQL STMT:" + selectStmt);
		ResultSet rs = stmt.executeQuery();
		
		if(rs.next()) {
			EmployeeTo result = new EmployeeTo(rs.getInt("EmployeeID"), 
											   rs.getString("LastName"), 
											   rs.getString("FirstName"), 
											   rs.getString("Title"),
											   rs.getString("Address"),
											   rs.getString("HireDate"));
			
			return result;
		}
		
		return null;
	}
	
	public int insertEmployee(String lastName,
			                  String firstName,
			                  String title,
			                  String address,
			                  String hireDate) throws SQLException{
		
		ActionsBD.lastIdCreated = -1;
		
		String insertStmt = "INSERT INTO Employees " + 
							"(LastName, FirstName, Title, Address, HireDate) " + 
							" VALUES (?, ?, ?, ?, ?) ";
		
		PreparedStatement stmt = ActionsBD.getConnection().prepareStatement(insertStmt);
		stmt.setString(1, firstName);
		stmt.setString(2, lastName);
		stmt.setString(3, title);
		stmt.setString(4, address); 
		stmt.setString(5, hireDate); 
		
		logger.info("SQL STMT:" + insertStmt);
		stmt.executeUpdate();
		
		ActionsBD.lastIdCreated = getLastIdCreated(); 
//		employeeTo.id = ActionsBD.lastIdCreated;				
		return ActionsBD.lastIdCreated;
	}
	
	public int getLastIdCreated() throws SQLException {
		
		String selectStmt = "SELECT seq FROM sqlite_sequence WHERE Name='Employees'";
		Statement stmt = ActionsBD.getConnection().createStatement();
		
		logger.info("SQL STMT:" + selectStmt);
		ResultSet result = stmt.executeQuery(selectStmt);
		
		if(result.next())
			return result.getInt("seq");
		
		return -1;
				
	}
	
	public int updateEmployee(int id,
			                  String lastName,
                              String firstName,
                              String title,
                              String address,
                              String hireDate) throws SQLException{
			 
		String updateStmt = "UPDATE Employees SET " + 
							"FirstName=?, " +
							"LastName=?, " +
							"Title=?, " + 
							"Address=?, " +
							"HireDate=?" +
							"WHERE " + 
							"EmployeeID=?";
		
		PreparedStatement stmt = ActionsBD.getConnection().prepareStatement(updateStmt);	
		stmt.setString(1, firstName);
		stmt.setString(2, lastName);
		stmt.setString(3, title);
		stmt.setString(4, address); 
		stmt.setString(5, hireDate); 
		stmt.setInt(6, id);
		
		logger.info("SQL STMT:" + updateStmt);
		
		return stmt.executeUpdate();
									
	}
	
	public int deleteEmployee(int id) throws SQLException {
		
		String deleteStmt = "DELETE FROM Employees WHERE EmployeeID=?";
		
		PreparedStatement stmt = ActionsBD.getConnection().prepareStatement(deleteStmt);
		stmt.setInt(1, id);
		
		logger.info("SQL STMT:" + deleteStmt);
		
		return stmt.executeUpdate();
	}


}
