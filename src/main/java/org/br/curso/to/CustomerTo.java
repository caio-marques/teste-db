package org.br.curso.to;

public class CustomerTo {
	
	public int id;
	public String companyName;
	public String contactName;
	public String contactTitle;
	public String address;
	public String city;
	public String state;

	public CustomerTo(int id, 
						   String companyName, 
						   String contactName, 
						   String address, 
						   String city, 
						   String state){
		this.id = id;
		this.companyName = companyName;
		this.contactName = contactName;
		this.address = address;
		this.city = city;
		this.state = state;
	}

}
