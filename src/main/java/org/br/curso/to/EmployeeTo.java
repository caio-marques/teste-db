package org.br.curso.to;


public class EmployeeTo {
	
	public int id;
	public String lastName;
	public String firstName;
	public String title;
	public String address;
	//a data é string pois no banco é um VARCHAR(25)
	public String hireDate;

	public  EmployeeTo() {
		
	}
	
	public  EmployeeTo (int id, 
					   String lastName,
					   String firstName,
					   String title,
					   String address,
					   String hireDate) {
		
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.title = title;
		this.address = address;
		this.hireDate = hireDate;
		
	}

}
